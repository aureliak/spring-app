package pl.keto.ketoapp.repository;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.model.User;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
//    @Query("select product.* from Product where userId = ?")
    List<Product> findProductByUser(User user);
    Product findProductById(int id);
}
