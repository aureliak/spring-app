window.onload = function () {
    var fat = document.getElementById('fat').value;
    var protein = document.getElementById('protein').value;
    var carbohydrates = document.getElementById('carbohydrates').value;
    var ctx = document.getElementById('myChart').getContext('2d');
    var doughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Tłuszcz', 'Białko', 'Węglowodany'],
            datasets: [{
                data: [fat, protein, carbohydrates],
                backgroundColor: [
                    'rgba(255,0,0,0.5)',
                    'rgba(0,255,0,0.5)',
                    'rgba(0,0,255,0.5)'
                ],
            }]
        }
    });
}