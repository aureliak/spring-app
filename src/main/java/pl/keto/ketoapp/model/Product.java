package pl.keto.ketoapp.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@Data
@RequiredArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @ManyToOne
    private User user;
    @NotNull
//    @Pattern(regexp = "^[0-9]*(.|,)[0-9]*$", message = "Number is required")
    private double kcal;
    @NotNull(message = "cant be null")
    private double protein;
    @NotNull(message = "cant be null")
    private double fat;
    @NotNull(message = "cant be null")
    private double carbohydrates;

    public Product(String name, User user, double kcal, double protein, double fat, double carbohydrates) {
        this.name = name;
        this.user = user;
        this.kcal = kcal;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
    }
}
