package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.keto.ketoapp.model.Date;
import pl.keto.ketoapp.model.Meal;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.MealRepository;
import pl.keto.ketoapp.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;


import org.springframework.ui.Model;
import pl.keto.ketoapp.services.MealService;


@Slf4j
@Controller
@RequestMapping("/")
public class MainController {

    private MealService mealService;
    private MealRepository mealRepository;
    private UserRepository userRepository;

    @Autowired
    public MainController(MealService mealService, MealRepository mealRepository, UserRepository userRepository) {
        this.mealService = mealService;
        this.mealRepository = mealRepository;
        this.userRepository = userRepository;
    }

    @GetMapping
    public String mainView(HttpSession session, HttpServletRequest request, Model model) {
        User user1 = new User();
        model.addAttribute("user", user1);

        if (session == null || !request.isRequestedSessionIdValid()) {
            log.info("You are not at session");
        } else {
            model.addAttribute("data", new Date());
            User user = (User) session.getAttribute("logStatus");
            User sessionUser = userRepository.getUserByUsername(user.getUsername());
            log.info("you are at session");
            LocalDate localDate = LocalDate.now();
            model.addAttribute("localDate", localDate);

            List<Meal> meals = mealRepository.findMealByUserAndDate(sessionUser, localDate);
            model.addAttribute("mealList", meals);
            mealService.getAllDataToProgress(model, meals);
            List<LocalDate> allDate = mealRepository.getAllDate(sessionUser);
            model.addAttribute("dates", allDate);
        }
        return "home";
    }


    @PostMapping
    public String chooseDate(@ModelAttribute Date date, HttpSession session, Model model) {
        try {
            model.addAttribute("data", new Date());
            User user = (User) session.getAttribute("logStatus");
            User sessionUser = userRepository.getUserByUsername(user.getUsername());
            System.out.println(date + " localdate");
            List<LocalDate> allDate = mealRepository.getAllDate(sessionUser);
            model.addAttribute("dates", allDate);
            LocalDate choosedDate = LocalDate.parse(date.getLocal());
            List<Meal> meals = mealRepository.findMealByUserAndDate(sessionUser, choosedDate);
            model.addAttribute("mealList", meals);
            mealService.getAllDataToProgress(model, meals);
            return "home";
        } catch (RuntimeException e) {
            return "redirect:/";
        }
    }

    @PostMapping(value = "/deleteMeal")
    public String handleDeleteMeal(Meal meal) {
        mealRepository.deleteById(meal.getMealid());
        return "redirect:/product";
    }
}






