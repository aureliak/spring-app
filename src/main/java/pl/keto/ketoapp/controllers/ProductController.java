package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.keto.ketoapp.model.Meal;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.MealRepository;
import pl.keto.ketoapp.repository.ProductRepository;
import pl.keto.ketoapp.repository.UserRepository;
import pl.keto.ketoapp.services.Productservice;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/product")
public class ProductController {

    private ProductRepository productRepository;

    private UserRepository userRepository;

    private MealRepository mealRepository;
    private Productservice productservice;

    @Autowired
    public ProductController(ProductRepository productRepository, UserRepository userRepository, MealRepository mealRepository, Productservice productservice) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.mealRepository = mealRepository;
        this.productservice = productservice;
    }

    @GetMapping
    private String view(Model model, HttpSession session) {
        Meal meal = new Meal();
        model.addAttribute("meal", meal);
        Product product = new Product();
        model.addAttribute("product", product);
        log.info("created product" + product);
        User user = (User) session.getAttribute("logStatus");
        User userByUsername = userRepository.getUserByUsername(user.getUsername());
        List<Product> all = productRepository.findProductByUser(userByUsername);
        model.addAttribute("listProd", all);
        return "product";
    }

    @PostMapping
    private String addProduct(@ModelAttribute @Valid Product product, Errors errors,Model model, HttpSession session) {
        if(errors.hasErrors()) {
            return "product";
        }else{
            User user = (User) session.getAttribute("logStatus");
            log.info("username " + user);
            User userByUsername = userRepository.getUserByUsername(user.getUsername());
            product.setUser(userByUsername);
            productRepository.save(product);
            log.info("Product saved" + product);
            List<Product> all = productRepository.findProductByUser(userByUsername);
            model.addAttribute("listProd", all);
            return "redirect:/product";
        }
    }

    @PostMapping(value = "/deleteProd")
    public String handleDeleteUser(Product product) {
        productservice.deleteProduct(product);
        return "redirect:/product";
    }


    @PostMapping("/addMeal")
    public String createDailyMeal(@ModelAttribute Meal meal, @Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "product";
        } else {
            product.getId();
            Product productFinded = productRepository.findProductById(product.getId());
            meal.setDate(LocalDate.now());
            meal.setProduct(productFinded);
            meal.setDate(LocalDate.now());
            mealRepository.save(meal);
            log.info(meal.toString());
            return "redirect:/product";
        }

    }
}


//added possibility to choose date and display table your meals from choosed date