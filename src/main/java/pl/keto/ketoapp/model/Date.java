package pl.keto.ketoapp.model;

import org.springframework.stereotype.Component;

@Component
public class Date {

    private String local;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    @Override
    public String toString() {
        return "Date{" +
                "local='" + local + '\'' +
                '}';
    }
}
