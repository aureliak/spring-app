package pl.keto.ketoapp.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.keto.ketoapp.model.Meal;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.model.User;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MealRepository extends CrudRepository<Meal, Integer> {
    Meal findById(int id);

    List<Meal> findAll();

    //    List<Meal> findMealByProduct(Prodyct user);
    List<Meal> findMealByProduct(Product product);

//    Meal findMealByProductUser(User user);

    @Query("select m.product from Meal m where m.product.user = :user")
    List<Product> findProductByUserId(@Param("user") User user);

    @Query("select m from Meal m where m.product.user = :user and m.date = :date")
    List<Meal> findMealByUserAndDate(@Param("user") User user, @Param("date") LocalDate localDate);

    @Query("select distinct m.date from Meal m where m.product.user = :user")
    List<LocalDate> getAllDate(@Param("user") User user);
}
