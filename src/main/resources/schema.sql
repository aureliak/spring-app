DROP SCHEMA IF EXISTS `keto-diet`;
CREATE SCHEMA `keto-diet`;
USE `keto-diet`;
drop table if exists `meal`;
drop table if exists `product`;
drop table if exists `user`;
SET character_set_client = utf8mb4;
USE `keto-diet`;
CREATE TABLE `user`
(
    `id`                  int(11)     NOT NULL AUTO_INCREMENT,
    `password`            varchar(45) NOT NULL,
    `username`            varchar(45) NOT NULL unique,
    `last_time_logged_in` DATE,
    `age`                 int(100),
    `height`              double,
    `weight`              double,
    `BMR`                 double,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;



CREATE TABLE `product`
(
    `id`            int(11)     NOT NULL AUTO_INCREMENT,
    `name`          varchar(45) NOT NULL,
    `kcal`          varchar(45) NOT NULL,
    `protein`       varchar(45) NOT NULL,
    `fat`           varchar(45) NOT NULL,
    `carbohydrates` varchar(45) NOT NULL,
    `user_id`       int(45)     NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES user (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;


CREATE TABLE `meal`
(
    `mealid`     int(11)  NOT NULL AUTO_INCREMENT,
    `count`      int(100) NOT NULL,
    `product_id` int(45)  NOT NULL,
    `date`       DATE     NOT NULL,
    PRIMARY KEY (`mealid`),
    FOREIGN KEY (`product_id`) REFERENCES product (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;

