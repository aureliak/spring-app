package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.UserRepository;

import javax.validation.Valid;


@Slf4j
@Controller
@RequestMapping("/sign-up")
public class SignUpController {

    UserRepository userRepository;

    @Autowired
    public SignUpController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    private String signUpView(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "signUp";
    }

    @PostMapping
    public String signUp(@Valid User user, Errors error, Model model) {
        try {
            if (error.hasErrors()) {
                return "signUp";
            } else if (user.getUsername() == null || user.getPassword() == null) {
                model.addAttribute("error", "Username already  exists");
                return "signUp";
            } else {
                userRepository.save(user);
                log.info("User created" + user);
                return "home";
            }

        } catch (DataIntegrityViolationException e) {
            log.info("Username already  exists");
            model.addAttribute("error", "Username already  exists");
            return "signUp";
        }
    }
}
