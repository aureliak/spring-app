package pl.keto.ketoapp;


import org.springframework.boot.SpringApplication;


import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KetoAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(KetoAppApplication.class, args);
    }


}
