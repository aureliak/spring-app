package pl.keto.ketoapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(schema = "keto-diet")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    @NotBlank(message = "Username is required")
    private String username;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Product> products;
    @NotBlank(message = "Password is required")
    private String password;

    @Column(name = "last_time_logged_in")
    private LocalDate lastTimeLoggedIn;
    @NotNull(message = "mus be eoe")
    private int age;
    @NotNull(message = "mus be eoe")
    private double height;
    @NotNull(message = "mus be eoe")
    private double weight;
    @NotNull(message = "mus be eoe")
    private double BMR;

    public User() {
    }

    public User(@NotBlank(message = "Username is required") String username, @NotBlank(message = "Password is required") String password,LocalDate lastLoggedIn) {
        this.username = username;
        this.password = password;
        this.lastTimeLoggedIn = lastLoggedIn;
    }

    public User(@NotNull(message = "mus be eoe") int age, @NotNull(message = "mus be eoe") double height, @NotNull(message = "mus be eoe") double weight, @NotNull(message = "mus be eoe") double BMR) {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.BMR = BMR;
    }

    public double getBMR() {
        return BMR;
    }

    public void setBMR(double BMR) {
        this.BMR = BMR;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public LocalDate getLastTimeLoggedIn() {
        return lastTimeLoggedIn;
    }

    public void setLastTimeLoggedIn(LocalDate lastLoggedIn) {
        this.lastTimeLoggedIn = lastLoggedIn;
    }


    public int getId() {
        return id;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    @Override
////    public String toString() {
////        try {
////            return "User{" +
////                    "id=" + id +
////                    ", username='" + username + '\'' +
////                    ", password='" + password + '\'' +
////                    '}';
////        } catch (Error e) {
////            e.getStackTrace();
////        }
////        return null;
////    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", lastTimeLoggedIn=" + lastTimeLoggedIn +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }
}
