package pl.keto.ketoapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import pl.keto.ketoapp.model.Meal;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.MealRepository;
import pl.keto.ketoapp.repository.UserRepository;

import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.List;

@Service
public class MealService {
    MealRepository mealRepository;
    UserRepository userRepository;

    @Autowired
    public MealService(MealRepository mealRepository, UserRepository userRepository) {
        this.mealRepository = mealRepository;
        this.userRepository = userRepository;
    }

//    public void getAllDataToProgress(Model model, HttpSession session, List<Meal> meals) {
////        DecimalFormat format = new DecimalFormat("#.##");
//        double sum1 = meals.stream()
//                .mapToDouble(meal -> ((meal.getProduct().getKcal() * meal.getCount()) / 100)).sum();
//        System.out.println(sum1);
//        double kcal = meals.stream().mapToDouble(userProduct -> (userProduct.getProduct().getKcal() *
//                userProduct.getCount()) / 100).sum();
//        model.addAttribute("kcal", Math.round(kcal));
//        double fat = meals.stream().mapToDouble(userProduct -> (userProduct.getProduct().getFat() *
//                userProduct.getCount()) / 100).sum();
//        model.addAttribute("fat", Math.round(fat * 10) / 10);
//        double carbohydrates = meals.stream().mapToDouble(userProduct -> (userProduct.getProduct().getCarbohydrates() *
//                userProduct.getCount()) / 100).sum();
//        model.addAttribute("carbohydrates", Math.round(carbohydrates * 10) / 10);
//        double protein = meals.stream().mapToDouble(userProduct -> (userProduct.getProduct().getProtein() *
//                userProduct.getCount()) / 100).sum();
//        model.addAttribute("protein", Math.round(protein * 10) / 10);
//        double fatPercentage = ((fat * 9) / kcal * 100);
//        model.addAttribute("fatPercentage", Math.round(fatPercentage));
//        double carbohydratesPercentage = ((carbohydrates * 4) / kcal * 100);
//        model.addAttribute("carbohydratesPercentage", Math.round(fatPercentage * 10) / 10);
//        double proteinPercentage = ((protein * 4) / kcal * 100);
//        model.addAttribute("proteinPercentage", Math.round(fatPercentage * 10) / 10);
//
//
//    }

    public void getAllDataToProgress(Model model, List<Meal> meals) {
        double kcal = meals.stream().mapToDouble
                (userProduct -> (userProduct.getProduct().getKcal() * userProduct.getCount()) / 100).sum();
        model.addAttribute("calories", Math.round(kcal * 10) / 10);
        double fat = meals.stream().mapToDouble
                (userProduct -> (userProduct.getProduct().getFat() * userProduct.getCount()) / 100).sum();
        model.addAttribute("fat", Math.round(fat * 10) / 10);
        double carbohydrates = meals.stream().mapToDouble
                (userProduct ->  (userProduct.getProduct().getCarbohydrates() * userProduct.getCount()) / 100).sum();
        model.addAttribute("carbohydrates", Math.round(carbohydrates * 10) / 10);
        double protein = meals.stream().mapToDouble
                (userProduct -> (userProduct.getProduct().getProtein() * userProduct.getCount()) / 100).sum();
        model.addAttribute("protein", Math.round(protein * 10) / 10);
        double fatPercentage = ((fat * 9) / kcal * 100);
        model.addAttribute("fatPercentage", Math.round(fatPercentage));
        double carbohydratesPercentage = ((carbohydrates * 4) / kcal * 100);
        model.addAttribute("carbPercentage", Math.round(carbohydratesPercentage));
        double proteinPercentage = ((protein * 4) / kcal * 100);
        model.addAttribute("protPercentage", Math.round(proteinPercentage));
    }
}