package pl.keto.ketoapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.keto.ketoapp.model.Meal;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.repository.MealRepository;
import pl.keto.ketoapp.repository.ProductRepository;

import java.util.List;
@Service
public class Productservice {
    MealRepository mealRepository;
    ProductRepository productRepository;

    @Autowired
    public Productservice(MealRepository mealRepository, ProductRepository productRepository) {
        this.mealRepository = mealRepository;
        this.productRepository = productRepository;
    }

    public void deleteProduct(Product product) {
        List<Meal> userProductList = mealRepository.findMealByProduct(product);
        if (userProductList.isEmpty()) {
            productRepository.deleteById(product.getId());
        } else {
            userProductList.forEach(mealRepository::delete);
            productRepository.deleteById(product.getId());
        }
    }
}
