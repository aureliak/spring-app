package pl.keto.ketoapp.mapper;

import org.springframework.jdbc.core.RowMapper;
import pl.keto.ketoapp.model.User;


import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper {



    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        User user = new User();
        user.setUsername(rs.getString("username"));
//        user.setPassword(rs.getString("password"));
        return user;
    }
}
