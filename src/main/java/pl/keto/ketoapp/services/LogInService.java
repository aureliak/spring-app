package pl.keto.ketoapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.UserRepository;

import javax.persistence.Query;
import java.util.List;

@Service
public class LogInService {
    UserRepository userRepository;

    @Autowired
    public LogInService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String isLoggedCorrect(User user) {
        if (user.getUsername() == "" || user.getPassword() == "") {
            return "empty";
        } else {
            List<String> list = userRepository.findUserByUsernameAndPassword(user.getUsername(), user.getPassword());
            if (list.isEmpty() == false) {
                return "correct";
            } else {
                return "not correct";
            }
        }
    }

}
