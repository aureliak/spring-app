package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.UserRepository;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.net.http.HttpRequest;
import java.text.NumberFormat;

@Controller
@Slf4j
@RequestMapping("/settings")
public class SettingsController {
    UserRepository userRepository;

    public SettingsController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    public String getForm(HttpSession session, Model model) {
        User user = (User) session.getAttribute("logStatus");
        log.info("username " + user);
        User sessionUser = userRepository.getUserByUsername(user.getUsername());
        model.addAttribute("user", sessionUser);
        System.out.println(user);
        System.out.println(sessionUser);
        return "settings";
    }

    @PostMapping("/changeBMR")
    public String calculateBMR(@ModelAttribute @Valid User user,BindingResult error, HttpSession session ) {
        if (error.hasErrors()) {
           error.getFieldErrors().forEach(fieldError -> System.out.println(fieldError.getField()));
            return "settings";
        } else {

            User sessionAttribute = (User) session.getAttribute("logStatus");
            User sessionUser = userRepository.getUserByUsername(sessionAttribute.getUsername());
            sessionUser.setAge(user.getAge());
            sessionUser.setWeight(user.getWeight());
            sessionUser.setHeight(user.getHeight());
            double resultBMR = (447.593 + (9.247 * sessionUser.getWeight()) + (3.098 * sessionUser.getHeight()) - (4.330 * sessionUser.getAge()));
            sessionUser.setBMR(resultBMR);
            System.out.println(sessionUser);
            userRepository.save(sessionUser);
            return "redirect:/settings";
        }

    }

    @PostMapping("/changePassword")
    public String changePassword(@ModelAttribute User user, HttpSession session, Model model) {
        User sessionAttribute = (User) session.getAttribute("logStatus");
        User sessionUser = userRepository.getUserByUsername(sessionAttribute.getUsername());
        if (user.getUsername().equals(sessionUser.getUsername())) {
            sessionUser.setPassword(user.getPassword());
            userRepository.save(sessionUser);
            return "redirect:/settings";
        } else {
            model.addAttribute("error", "bad username");
            log.info("Bad username for changing password");
            return "settings";
        }

    }

//    @GetMapping("/changePassword")
//    public String getView(){
//        return "redirect:/settings";
//    }
}
