package pl.keto.ketoapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.keto.ketoapp.model.User;

import javax.persistence.GeneratedValue;
import java.util.List;


public interface UserRepository extends JpaRepository<User, Integer> {
    User getUserByUsername(String username);

    @Query("select u.username from User u  where u.username = :username and u.password=:password")
    List<String> findUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
