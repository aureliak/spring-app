package pl.keto.ketoapp.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "meal")
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int mealid;
    @ManyToOne
    private Product product;
    private LocalDate date;
    private double count;

    public int getMealid() {
        return mealid;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public Meal() {
    }


    @Override
    public String toString() {
        return "Meal{" +
                "mealid=" + mealid +
                ", product=" + product +
                ", date=" + date +
                ", count=" + count +
                '}';
    }

    public void setMealid(int mealid) {
        this.mealid = mealid;
    }
}
