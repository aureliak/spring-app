package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.UserRepository;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;

@RequestMapping("/log-out")
@Controller
@Slf4j
public class LogOutController {
    UserRepository userRepository;

    @Autowired
    public LogOutController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    public String logOutview(HttpSession session) {

        User user = (User) session.getAttribute("logStatus");
        log.info("username " + user);
        User sessionUser = userRepository.getUserByUsername(user.getUsername());
        sessionUser.setLastTimeLoggedIn(LocalDate.now());
        userRepository.save(sessionUser);
        log.info("log out as session.getAttribute(‘logStatus’) " + session.getAttribute("logStatus"));
        session.invalidate();
        log.info("Session invalidate");
        return "redirect:/";
    }
}
