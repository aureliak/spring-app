package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.keto.ketoapp.model.User;
import pl.keto.ketoapp.repository.UserRepository;

import javax.servlet.http.HttpSession;
import java.net.BindException;

@Controller
@Slf4j
@RequestMapping("/firstLoggedIn")
public class FirstLoggedInController {
    UserRepository userRepository;

    public FirstLoggedInController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    public String getForm(HttpSession session, Model model) {
        User user = (User) session.getAttribute("logStatus");
        log.info("username " + user);
        User sessionUser = userRepository.getUserByUsername(user.getUsername());
        model.addAttribute("user", sessionUser);
        System.out.println(user);
        System.out.println(sessionUser);
        return "first";
    }

    @PostMapping
    public String addUserData(@ModelAttribute User user, HttpSession session) {
            User sessionAttribute = (User) session.getAttribute("logStatus");
            User sessionUser = userRepository.getUserByUsername(sessionAttribute.getUsername());
            sessionUser.setAge(user.getAge());
            sessionUser.setWeight(user.getWeight());
            sessionUser.setHeight(user.getHeight());
            userRepository.save(sessionUser);
            double resultBMR = (447.593 + (9.247 * sessionUser.getWeight()) + (3.098 * sessionUser.getHeight()) - (4.330 * sessionUser.getAge()));
            sessionUser.setBMR(resultBMR);
            userRepository.save(sessionUser);
            return "redirect:/";
    }
}
