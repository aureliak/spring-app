package pl.keto.ketoapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.keto.ketoapp.model.Product;
import pl.keto.ketoapp.model.User;

import pl.keto.ketoapp.repository.UserRepository;
import pl.keto.ketoapp.services.LogInService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/login")
public class LoginController {

    LogInService logInService;
    UserRepository userRepository;

    @Autowired
    public LoginController(LogInService logInService, UserRepository userRepository) {
        this.logInService = logInService;
        this.userRepository = userRepository;
    }

    @GetMapping
    public String logInView(Model model) {
        User user = new User();
        Product product = new Product();
        model.addAttribute("product", product);
        model.addAttribute("user", user);
        return "loginForm";
    }

    @PostMapping
    public String logInFrom(@ModelAttribute("user") @Valid User user, Errors errors, HttpServletRequest request, Model model) throws Throwable {
        String isLoggedCorrect = logInService.isLoggedCorrect(user);
        if (errors.hasErrors()) {
            return "loginForm";
        } else if (isLoggedCorrect == "not correct") {
            log.info("Bad username or password" + user);
            model.addAttribute("error", "Bad username or password");
            return "loginForm";
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("logStatus", user);
            Object sessionName = session.getAttribute("logStatus");
            log.info("session.getAttribute(‘logStatus’) " + sessionName);
            System.out.println(sessionName);
            User sessionUser = userRepository.getUserByUsername(user.getUsername());
            if (sessionUser.getLastTimeLoggedIn() == null) {
                log.info("user logged in for first time " + user);
                return "redirect:/firstLoggedIn";
            } else {
                log.info("user logged in" + user);
                log.info("session.getAttribute(‘logStatus’) " + sessionName);
                return "redirect:/";
            }
        }

    }
}
